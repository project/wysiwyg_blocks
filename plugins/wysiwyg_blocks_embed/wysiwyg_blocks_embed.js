(function ($) {

  Drupal.wysiwyg.plugins.wysiwyg_blocks_embed = {

    /**
     * Execute the button.
     */
    invoke: function (data, settings, instanceId) {

      if (data.content !== '') {
        settings.data = $(data.content).data('wysiwyg_blocks_embed');
      }

      this.show_popup(settings, instanceId);
    },

    /**
     * Replace all <!--wysiwyg_blocks_embed... --> tags with images.
     */
    attach: function (content, settings) {
      // Individually replace each widget so they have a chance to be identified.
      var dom = document.createElement('div');
      dom.innerHTML = content;
      var comments = this.getComments(dom, []);
      if (comments.length > 0) {
        for (var i = 0; i < comments.length; i++) {
          var comment = comments[i];
          if (comment.nodeValue.indexOf('wysiwyg_blocks_embed') === 0) {
            var data_value = JSON.parse(comment.nodeValue.replace('wysiwyg_blocks_embed', ''));
            var icon_markup = $(settings.icon_markup).get(0);
            icon_markup.dataset.wysiwyg_blocks_embed = JSON.stringify(data_value);
            comment.replaceWith(icon_markup);
            content = dom.innerHTML;
          }
        }
      }

      return content;

    },

    /**
     * Replace images with <!--widget_embed ... -->.
     */
    detach: function (content) {
      var jo = $('<div>' + content + '</div>');
      jo.find('img').each(function () {
        if ($(this).data('wysiwyg_blocks_embed')) {
          $(this).replaceWith($('<!--wysiwyg_blocks_embed ' + JSON.stringify($(this).data('wysiwyg_blocks_embed')) + '-->'));
        }
      });
      return jo.html();
    },

    /**
     * Shows the Form.
     */
    show_popup: function (settings, instanceId) {
      // Append form.
      $('body').append(settings.form_markup);

      $('.wysiwyg-blocks-embed').center().show('fast', 'linear', function() {
        var block = '';
        var module = '';

        // Listeners for buttons.
        $('#edit-wysiwyg-blocks-embed-close').click(function() {
          $('.wysiwyg-blocks-embed').remove();
        });

        if (settings.data !== undefined) {
          $('#edit-wysiwyg-blocks-embed-insert').val('Update');
          block = settings.data.delta;
          $('input[value="' + block  + '"]').prop('checked', true);
        }

        $('input[type="radio"]').click(function() {
          if (block !== '') {
            $('input[value="' + block  + '"]').prop('checked', false);
          }
          block = $(this).val();
          module = $(this).attr('name');
        });

        $('#edit-wysiwyg-blocks-embed-insert').click(function() {
          var content;

          if (block !== undefined) {
            var data_value = {
              'module': module,
              'delta': block
            };
            content = $(settings.icon_markup).get(0);
            content.dataset.wysiwyg_blocks_embed = JSON.stringify(data_value);
          }

          // Insert the placeholder.
          Drupal.wysiwyg.instances[instanceId].insert(content.outerHTML);

          $('.wysiwyg-blocks-embed').remove();
        });
      });
    },

    /**
     * Find the comment nodes.
     */
    getComments: function (node, comments) {
      node = node.firstChild;
      while (node) {
        if (node.nodeType === 8) {
          comments.push(node);
        }
        comments = this.getComments(node, comments);
        node = node.nextSibling;
      }
      return comments;
    },

  };

})(jQuery);

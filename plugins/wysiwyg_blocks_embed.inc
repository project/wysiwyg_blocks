<?php

/**
 * @file
 * Wysiwyg API integration.
 */

/**
 * Implements hook_wysiwyg_module_plugin().
 */
function wysiwyg_blocks_wysiwyg_blocks_embed_plugin() {

  $plugins['wysiwyg_blocks_embed'] = [
    'title' => t('WYSIWYG Blocks'),
    'icon file' => 'blocks.png',
    'icon title' => t('Blocks'),
    'settings' => [
      'form_markup' => wysiwyg_blocks_wysiwyg_form(),
      'placeholder_markup' => [
        'prefix' => '<!--wysiwyg_blocks_embed',
        'suffix' => '-->',
      ],
      'placeholders' => array(),
      'icon_markup' => '<img class="wysiwyg_blocks_embed" title="Widget embed" ' .
        'src="/' . drupal_get_path('module', 'wysiwyg_blocks') .
        '/plugins/wysiwyg_blocks_embed/images/blocks.png' . '" />',
    ],
  ];

  return $plugins;
}

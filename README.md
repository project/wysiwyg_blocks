# WYSIWYG Blocks
A wysiwyg widget for adding blocks through the wysiwyg.

## Installation
Install as normal.

* Enable `WYSIWYG Blocks` in the WYSIWYG profiles here: 
  admin/config/content/wysiwyg
* Edit the required filter and check the box 
  in the `Buttons and Plugins` section.
* Grant the "Administer WYSIWYG blocks" permission to allowed roles

## Usage
* Enable blocks that can be added via the admin form here:
`/admin/config/content/wysiwyg_blocks` or via the block form of an individual block via the checkbox.
* On the wysiwyg click on the wysiwyg_blocks icon: ![icon](https://github.com/dennisinteractive/wysiwyg_blocks/blob/master/plugins/wysiwyg_blocks_embed/images/blocks.png)
* Select the block from the list and click insert.
* Update the block by highlighting the inserted icon and clicking on the icon.

<?php
/**
 * @file
 * Admin form for wysiwyg blocks.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function wysiwyg_blocks_admin_form() {
  $form['wysiwyg_blocks_enabled'] = [
    '#title' => t('Enable blocks to be added in WYSIWYG.'),
    '#type' => 'fieldset',
  ];

  // Make the form a tree so that we can get the module statuses in a structured format.
  $form['#tree'] = TRUE;

  foreach (wysiwyg_blocks_get_all_blocks() as $module => $blocks) {
    // If one of the blocks is enabled don't collapse it.
    $values = wysiwyg_blocks_get_module_blocks_list($module);
    $collapsed = empty(array_filter($values));

    $form['wysiwyg_blocks_enabled'][$module] = [
      '#title' => t($module),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
    ];
    $form['wysiwyg_blocks_enabled'][$module]['blocks'] = [
      '#type' => 'checkboxes',
      '#options' => $blocks,
      '#default_value' => $values,
    ];
  }

  $form = system_settings_form($form);
  array_unshift($form['#submit'], 'wysiwyg_blocks_admin_form_submit');
  return $form;
}

/**
 * Submit handler for wysiwyg_blocks_admin_form().
 *
 * @param $form
 * @param $form_state
 */
function wysiwyg_blocks_admin_form_submit($form, &$form_state) {
  // Get short list of enabled blocks.
  $block_list = array();
  foreach ($form_state['values']['wysiwyg_blocks_enabled'] as $module => $fields) {
    if ($blocks = array_filter($fields['blocks'])) {
      $block_list[$module] = $blocks;
    }
  }
  variable_set('wysiwyg_blocks_enabled', $block_list);

  // Unset tree fields so that they aren't resaved in system_settings_form_submit().
  unset($form_state['values']['wysiwyg_blocks_enabled']);
}
